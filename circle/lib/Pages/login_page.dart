
import 'package:circle/Utils/responsive.dart';
import 'package:circle/Widgets/Home.dart';
import 'package:circle/Widgets/icon_container.dart';
import 'package:circle/Widgets/login_form.dart';
import 'package:flutter/material.dart';

class login_page extends StatefulWidget {
  static const route_name = 'login';
  _login_pageState createState() => _login_pageState();
}

class _login_pageState extends State<login_page> {
  @override
  Widget build(BuildContext context) {
    final responsive respon = responsive.of(context);
    final double pinkwidth   =   respon.wp(80); 
    final double purplewidth   = respon.wp(60);
    return Scaffold(
      body: GestureDetector(
        onTap: (){
          FocusScope.of(context).unfocus();
      },
        child: SingleChildScrollView(
          child: Container(
            width: double.infinity,
            height: respon.heigth,
            color: Colors.white,
            child: Stack(
              alignment: Alignment.center,
              children: <Widget>[
                Positioned(
                    right: -(pinkwidth)*0.2,
                    top: -(pinkwidth)*0.4,
                    child: Home(
                      size: pinkwidth,
                      colors: [
                        Colors.pink,
                        Colors.pinkAccent
                      ],
                    )
                ),
                Positioned(
                    left: -(purplewidth)*0.2,
                    top: -(purplewidth)*0.5,
                    child: Home(
                      size: purplewidth,
                      colors: [
                        Colors.purple,
                        Colors.purpleAccent,
                      ],
                    )
                ),
                Positioned(
                    top: pinkwidth*0.35,
                    child: Column(
                      children: <Widget> [
                        IconContainer(
                          size: respon.wp(17),
                        ),
                        SizedBox(
                          height: respon.dp(4),
                        ),
                        Text("Inicia Sesion \n Bienvenido! ",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: respon.wp(4),
                          ),
                        ),
                      ],
                    )
                ),
                loginform()
              ],
            ),
          ),
        ),
      ),
    );
  }
}
