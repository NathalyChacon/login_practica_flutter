
import 'package:circle/Utils/responsive.dart';
import 'package:circle/Widgets/Home.dart';
import 'package:circle/Widgets/avatar_button.dart';
import 'package:circle/Widgets/icon_container.dart';
import 'package:circle/Widgets/login_form.dart';
import 'package:circle/Widgets/register_form.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class register_page extends StatefulWidget {
  static const routeName = 'register';
  _register_pageState createState() => _register_pageState();
}

class _register_pageState extends State<register_page> {
  @override
  Widget build(BuildContext context) {
    final responsive respon = responsive.of(context);
    final double pinkwidth   =   respon.wp(80);
    final double purplewidth   = respon.wp(60);
    return Scaffold(
      body: GestureDetector(
        onTap: (){
          FocusScope.of(context).unfocus();
        },
        child: SingleChildScrollView(
          child: Container(
            width: double.infinity,
            height: respon.heigth,
            color: Colors.white,
            child: Stack(
              alignment: Alignment.center,
              children: <Widget>[
                Positioned(
                    right: -(pinkwidth)*0.2,
                    top: -(pinkwidth)*0.2,
                    child: Home(
                      size: pinkwidth,
                      colors: [
                        Colors.pink,
                        Colors.pinkAccent
                      ],
                    )
                ),
                Positioned(
                    left: -(purplewidth)*0.2,
                    top: -(purplewidth)*0.3,
                    child: Home(
                      size: purplewidth,
                      colors: [
                        Colors.purple,
                        Colors.purpleAccent,
                      ],
                    )
                ),
                Positioned(
                    top: pinkwidth*0.25,
                    child: Column(
                      children: <Widget> [
                        Text("Hola! \n Registrate. ",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: respon.wp(4),
                            color: Colors.white,
                          ),
                        ),
                        SizedBox(height: respon.dp(5)),
                        avatar_button(
                          imageSize: respon.wp(25),
                        ),
                      ],
                    )
                ),
                register_form(),
                Positioned(
                  left: 15,
                  top: 15,
                  child: SafeArea(child:
                  CupertinoButton(
                    color: Colors.black26,
                    padding: EdgeInsets.all(10),
                    borderRadius: BorderRadius.circular(30),
                    child: Icon(Icons.arrow_back),
                    onPressed: (){
                      Navigator.pop(context);
                    },
                  ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
