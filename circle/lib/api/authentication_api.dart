
import 'package:circle/helpers/http_response.dart';
import 'package:dio/dio.dart';
import 'package:logger/logger.dart';
import 'package:meta/meta.dart' show required;

class authentication_api {
  final Dio _dio = Dio();
  final Logger _logger = Logger();
  
  Future<http_response> register({
    @required String username,
    @required String password,
    @required String email,
}) async {
    try{

    final response = await _dio.post(
      'https://curso-api-flutter.herokuapp.com/api/v1/register',
    options: Options(
      headers: {
        'Content-Type' : 'application/json',
      },
    ),
      data: {
        "username" : username,
        "email" : email,
        "password": password,
      }
    ); return http_response.success(response.data);
    _logger.i(response.data);
    }catch(e){
      _logger.e(e);

      int statusCode = -1;
      String message = 'error desconocido';
      dynamic data = null;
      if(e is DioError){
        message = e.message;
        if(e.response!=null){
          statusCode = e.response.statusCode;
          message = e.response.statusMessage;
          data = e.response.data;
        }
      }
      return http_response.fail(statusCode: statusCode, message: message, data: data);
    }
  }
}