

import 'package:flutter/material.dart';

class Home extends StatelessWidget {
  final double size;
  final List<Color> colors;

  Home({Key key, @required this.size, @required this.colors})
      : assert(size != null && size > 0),
        assert(colors != null && colors.length>=2),
        super(key: key);

  Widget build(BuildContext context) {
    return Container(
      width: size,
      height: size,
      decoration: BoxDecoration(
        color: Colors.purple,
        shape: BoxShape.circle,
        gradient: LinearGradient(
            colors: this.colors,
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter
        )
      ),
    );
  }
}
