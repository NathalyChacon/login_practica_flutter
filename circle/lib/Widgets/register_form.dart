import 'dart:convert';

import 'package:circle/Pages/login_page.dart';
import 'package:circle/Pages/register_page.dart';
import 'package:circle/Utils/dialogs.dart';
import 'package:circle/Utils/responsive.dart';
import 'package:circle/Widgets/dashboard.dart';
import 'package:circle/Widgets/input_text.dart';
import 'package:circle/api/authentication_api.dart';
import 'package:circle/helpers/http_response.dart';
import 'package:flutter/material.dart';
import 'package:logger/logger.dart';

class register_form extends StatefulWidget {
  @override
  _register_formState createState() => _register_formState();
}

class _register_formState extends State<register_form> {

  GlobalKey<FormState> _formkey = GlobalKey();
  String _email = '' , _password = '' , _username = '';
  final authentication_api _api = authentication_api();
  Logger _logger = Logger();

  Future<void> _submit() async{
    final isOk = _formkey.currentState.validate();
    if(isOk){
      progress_dialog.show(context);
      http_response response = await  _api.register(username: _username, password: _password, email: _email);
      progress_dialog.dissmiss(context);
      
      if(response.data!=null){
        _logger.e("register ok::: ${response.data}");
        Navigator.pushNamedAndRemoveUntil(context, dashboard.routeName,(_)=>false);
      }else{
        _logger.e("register fail ${response.error.statusCode}");


        String message = response.error.message;


        if( response.error.statusCode == -1){
          message = "No tienes internet";
        }else if( response.error.statusCode == 409){
          message = "Usuario ya registrado ${jsonEncode(response.error.data['duplicatedFields'])}";
        }
        dialogs.alert(
            context,
            title: "Error",
            description: message
        );
      }
   }
  }

  @override
  Widget build(BuildContext context) {
    final responsive respon = responsive.of(context);
    return Positioned(
      bottom: 30,
      child: Container(
        constraints: BoxConstraints(
          maxWidth: respon.isTablet ? 430 : 300,
        ),
        child: Form(
          key: _formkey,
          child: Column(
            children:<Widget>[
              input_text(
                keyboardType: TextInputType.emailAddress,
                label: "Username",
                fontSize: respon.dp(1.5),
                onChanged: (text){
                  _username = text;
                },
                validator: (text){
                  if(text.trim().length < 5){
                    return "Username Invalido.";
                  }
                  return null;
                },

              ),

              input_text(
                keyboardType: TextInputType.emailAddress,
                label: "Email",
                fontSize: respon.dp(1.5),
                onChanged: (text){
                  _email = text;
                },
                validator: (text){
                  if(!text.contains("@")){
                    return "Email Invalido.";
                  }
                  return null;
                },

              ),
              Container(
                decoration: BoxDecoration(
                  border: Border(
                    bottom: BorderSide(
                        color: Colors.black12
                    ),
                  ),
                ),
                child: Row(
                  children: <Widget>[
                    Expanded(child:
                    input_text(
                      label: "Password",
                      obscureText: true,
                      borderEnabled: false,
                      fontSize: respon.dp(1.5),
                      onChanged: (text){
                        _password = text;
                      },
                      validator: (text){
                        if(text.trim().length < 2){
                          return "Porfavor ingrese una contraseña";
                        }
                      },
                    ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: respon.dp(4)),
              SizedBox(
                width: double.infinity,
                child: MaterialButton(
                  padding: EdgeInsets.symmetric(vertical: 15),
                  child: Text("Registrate", style: TextStyle(color: Colors.white, fontSize: respon.dp(1.5)),),
                  onPressed: this._submit,
                  color: Colors.pinkAccent,
                ),
              ),
              SizedBox(height: respon.dp(2)),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text("Ya tienes una cuenta?", style: TextStyle(fontSize: respon.dp(1.5)
                  ),
                  ),
                  MaterialButton(
                    child: Text("Inicia ahora",
                      style: TextStyle(
                          color: Colors.pinkAccent,
                          fontSize: respon.dp(1.5)
                      ),
                    ),
                    onPressed: (){
                      Navigator.pushNamed(context, login_page.route_name);
                    },
                  ),
                ],
              ),
              SizedBox(height: respon.dp(10)),
            ],
          ),
        ),
      ),
    );
  }
}




