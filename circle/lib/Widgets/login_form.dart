import 'package:circle/Pages/register_page.dart';
import 'package:circle/Utils/responsive.dart';
import 'package:circle/Widgets/input_text.dart';
import 'package:flutter/material.dart';

class loginform extends StatefulWidget {
  @override
  _loginformState createState() => _loginformState();
}

class _loginformState extends State<loginform> {

  GlobalKey<FormState> _formkey = GlobalKey();
  String _email = '' , _password = '';


  _submit(){
final isOk = _formkey.currentState.validate();
if(isOk){}
            }

  @override
  Widget build(BuildContext context) {
    final responsive respon = responsive.of(context);
    return Positioned(
      bottom: 30,
        child: Container(
          constraints: BoxConstraints(
            maxWidth: respon.isTablet ? 430 : 300,
          ),
          child: Form(
            key: _formkey,
            child: Column(
              children:<Widget>[
              input_text(
                keyboardType: TextInputType.emailAddress,
                label: "EMAIL",
                fontSize: respon.dp(1.5),
                onChanged: (text){
                  _email = text;
                },
                validator: (text){
                  if(!text.contains("@")){
                    return "Email Invalido.";
                  }
                  return null;
                },

              ),
              Container(
                decoration: BoxDecoration(
                  border: Border(
                    bottom: BorderSide(
                      color: Colors.black12
                    ),
                  ),
                ),
                child: Row(
                 children: <Widget>[
                  Expanded(child:
                  input_text(
                    label: "PASSWORD",
                    obscureText: true,
                    borderEnabled: false,
                    fontSize: respon.dp(1.5),
                    onChanged: (text){
                      _password = text;
                    },
                    validator: (text){
                      if(text.trim().length==0){
                        return "Porfavor ingrese una contraseña";
                      }
                    },
                  ),
                  ),
                   FlatButton(
                     padding: EdgeInsets.symmetric(vertical: 2),
                     child: Text("Forgot Password",
                       style: TextStyle(
                           fontWeight: FontWeight.bold,
                            fontSize: respon.dp(1.7)
                       ),
                     ),
                   onPressed: (){},
                   )
                 ],
                ),
              ),
                SizedBox(height: respon.dp(4)),
                SizedBox(
                  width: double.infinity,
                  child: MaterialButton(
                    padding: EdgeInsets.symmetric(vertical: 15),
                  child: Text("Inicia Sesion", style: TextStyle(color: Colors.white, fontSize: respon.dp(1.5)),),
                  onPressed: this._submit,
                  color: Colors.pinkAccent,
                ),
                ),
                SizedBox(height: respon.dp(2)),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                  Text("Quieres ser parte de nosotros?", style: TextStyle(fontSize: respon.dp(1.5)
                  ),
                  ),
                  MaterialButton(
                    child: Text("Registrate",
                      style: TextStyle(
                          color: Colors.pinkAccent,
                          fontSize: respon.dp(1.5)
                      ),
                    ),
                    onPressed: (){
                      Navigator.pushNamed(context, register_page.routeName);
                    },
                  ),
                ],
                ),
                SizedBox(height: respon.dp(10)),
              ],
             ),
          ),
        ),
        );
  }
}




