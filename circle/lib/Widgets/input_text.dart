
import 'package:flutter/material.dart';

class input_text extends StatelessWidget {
final String label;
final TextInputType keyboardType;
final bool obscureText, borderEnabled;
final double fontSize;
final void Function(String text) onChanged;
final void Function(String text) validator;

const input_text({
  Key key,
  this.label = '',
  this.keyboardType = TextInputType.text,
  this.obscureText = false,
  this.borderEnabled = true,
  this.fontSize = 1,
  this.onChanged,
  this.validator,
}) : super(key: key);

  Widget build(BuildContext context) {
    return TextFormField(
      keyboardType: this.keyboardType,
      obscureText: this.obscureText,
      onChanged: this.onChanged,
      validator: this.validator,
      style: TextStyle(fontSize: this.fontSize),
      decoration: InputDecoration(
          labelText: this.label,
          contentPadding: EdgeInsets.symmetric(vertical: 5),
          enabledBorder: this.borderEnabled?UnderlineInputBorder(
            borderSide: BorderSide(
              color: Colors.black12,
            )
          ) : InputBorder.none,
          labelStyle: TextStyle(
              color: Colors.black26,
              fontWeight: FontWeight.w500
          ),
      ),
    );
  }
}

