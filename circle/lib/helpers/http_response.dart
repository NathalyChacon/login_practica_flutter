import 'package:meta/meta.dart' show required;

class http_response{
  final dynamic data;
  final http_error error;

  http_response(this.data, this.error);

  static http_response success(dynamic data) => http_response(data, null);
  static http_response fail({
    @required int statusCode,
    @required String message,
    @required dynamic data,
}) => http_response(
      null,
      http_error(statusCode:statusCode, message:message, data:data)
  );
}

class http_error{
  final int statusCode;
  final String message;
  final dynamic data;

  http_error({
        @required this.statusCode,
        @required this.message,
        @required this.data
      });
}