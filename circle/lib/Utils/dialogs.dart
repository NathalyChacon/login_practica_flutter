

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

abstract class dialogs {
  static alert(
      BuildContext context, {
        @required String title,
        @required String description
      }){
    showDialog(
      context: context,
      builder: (_)=>AlertDialog(
        title: Text(title),
        content: Text(description),
        actions: [
          FlatButton(onPressed: (){Navigator.pop(_);
          },
              child: Text("ok"),
          )
        ],
    ),
    );
  }
}

abstract class progress_dialog{

  static show(BuildContext context){
    showCupertinoModalPopup(
        context: context,
        builder: (_){
          return WillPopScope(child: Container(
            width: double.infinity,
            height: double.infinity,
            color: Colors.black26.withOpacity(0.2),
            child: Center(
              child: CircularProgressIndicator(

              ),
            ),
          ), onWillPop: () async => false,
          );
        }
    );
  }

  static dissmiss(BuildContext context){
    Navigator.pop(context);
  }
}